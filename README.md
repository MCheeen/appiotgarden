# WIP New Project 2.0

Project Team:  
Naiane 

Marcos Chen  

Samuel Ducca

Victor Dias 

## Screen Shots
<img width="217" alt="Captura de Tela 2020-08-11 às 09 45 20" src="https://user-images.githubusercontent.com/22487037/134823350-ab4aad59-9a7a-41d7-a240-ebe9ef70d30a.jpeg">
<img width="217" alt="Captura de Tela 2020-08-11 às 09 45 20" src="https://user-images.githubusercontent.com/22487037/134823352-35dd9e22-1461-4032-8f8c-516ccd1f20b3.jpeg">
<img width="217" alt="Captura de Tela 2020-08-11 às 09 45 20" src="https://user-images.githubusercontent.com/22487037/134823355-7150b551-d47f-4f83-9849-442baefa65ee.jpeg">


# RELEASE 1.0
Projeto IoT  - PCS 3734 - 2020
Sistema de monitoramento de jardins

React native project for Network Laboratory PCS 3734 - 2020 of Polytechnic School of the University of São Paulo  
System of monitoring umity and temperature from sensors that stores in a AWS DynamoBD and serves the app

The server project https://github.com/gludescher/iot_garden_aws and https://github.com/gludescher/iot_garden_backend

Project Team:  
Guilherme Ludescher  
Marcos Chen  
Matheus Sato  
Victor Dias  

## Tecnologies
React Native CLI  
Typescript  
React Native SQL Lite and Async Storage for storage  
Charts using React Native Charts Kit  

## To Start Project
1 Install the enviroment to run react-native project  
2 yarn install on the project directory  

For Android  
3 react-native link  
4 yarn android    

For iOS  
3 cd ios  
4 pod install  
5 cd..  
6 yarn ios  

## Project Screen Shots  
<img width="217" alt="Captura de Tela 2020-08-11 às 09 45 20" src="https://user-images.githubusercontent.com/22487037/89898610-60ef1200-dbb7-11ea-85ed-b22a5955e1e7.png">  
<img width="590" alt="Captura de Tela 2020-08-11 às 09 45 44" src="https://user-images.githubusercontent.com/22487037/89898639-6f3d2e00-dbb7-11ea-9972-31ae92737361.png">
<img width="579" alt="Captura de Tela 2020-08-11 às 09 45 55" src="https://user-images.githubusercontent.com/22487037/89898657-75cba580-dbb7-11ea-978b-3eac9119eb42.png">
<img width="568" alt="Captura de Tela 2020-08-11 às 09 47 16" src="https://user-images.githubusercontent.com/22487037/89898795-b0cdd900-dbb7-11ea-9db3-714900ee733e.png">
