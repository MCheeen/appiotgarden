import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  menuContainer: {
    marginTop: 15
  },
  title: {
    fontSize: 30, paddingLeft: 15, fontWeight: 'bold'
  },
  scrollView: {
    backgroundColor: 'white'
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
});