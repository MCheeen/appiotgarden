export const MockedFetch = {
  "login": "marcostcchen",
  "nome": "Marcos Chen",
  "plantacoes": {
    "1": {
      "planta": "Beterraba",
      "sensores": {
        "1": {
          "medicoes": {
            "1595885881": {
              "temp": "25"
            },
            "1595885895": {
              "temp": "23"
            }
          },
          "tipoSensor": "temp"
        },
        "2": {
          "medicoes": {
            "1595886002": {
              "umid": "21"
            },
            "1595886003": {
              "umid": "30"
            },
            "1595886004": {
              "umid": "33"
            }
          },
          "tipoSensor": "umid"
        },
        "3": {
          "medicoes": {
            "1695885895": {
              "umidsolo": "40"
            },
            "1695985881": {
              "umidsolo": "38"
            }
          },
          "tipoSensor": "umidsolo"
        }
      }
    },
    "2": {
      "planta": "Alface",
      "sensores": {
        "1": {
          "medicoes": {
            "1595885923": {
              "umid": "35"
            },
            "1595985923": {
              "umid": "55"
            },
            "1596085923": {
              "umid": "30"
            },
            "1596185923": {
              "umid": "50"
            },
            "1596285923": {
              "umid": "40"
            }
          },
          "tipoSensor": "umid"
        },
        "2": {
          "medicoes": {
            "1695885881": {
              "temp": "35"
            },
            "1695885895": {
              "temp": "33"
            }
          },
          "tipoSensor": "temp"
        }
      }
    },
    "3": {
      "planta": "Repolho",
      "sensores": {
        "1": {
          "medicoes": {
            "1595885923": {
              "umid": "90"
            },
            "1595985923": {
              "umid": "55"
            },
            "1596085923": {
              "umid": "80"
            },
            "1596185923": {
              "umid": "50"
            },
            "1596285923": {
              "umid": "70"
            }
          },
          "tipoSensor": "umid"
        },
        "2": {
          "medicoes": {
            "1695885895": {
              "umidsolo": "50"
            },
            "1695985881": {
              "umidsolo": "68"
            }
          },
          "tipoSensor": "umidsolo"
        }
      }
    }
  }
}